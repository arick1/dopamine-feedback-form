import { Route, Routes } from 'react-router-dom';
import './App.css';
import ReviewState from './components/context/ReveiwState';
import Feedback from './components/feedback/Feedback';
import Header from './components/header/Header';
import NotFound from './components/notFound/NotFound';
import Protected from './components/protected/Protected';
import Success from './components/success/Success';

function App() {
  return (
    <div className='App'>
      <ReviewState>
        <Header />
        <Routes>
          <Route index element={<Feedback />}/>
          <Route path= "/success" element={<Protected><Success /></Protected>}/>
          <Route path='*' element={<NotFound />}/>
        </Routes>
      </ReviewState>
    </div>
  );
}

export default App;
