import { useContext } from "react";
import ReactStars from "react-rating-stars-component";
import ReveiwContext from "../context/ReviewContext";
import "./Ratings.css";

const Ratings = () => {
  const {setFirstRatings, setSecondRatings} = useContext(ReveiwContext);

  return (
    <div className="ratings">
      <div className="box">
        <p>How was your treatment sessions ?</p>
        <ReactStars
          className={`stars`}
          count={5}
          onChange={(value) =>
            setFirstRatings(value)
          }
          size={50}
          isHalf={false}
          emptyIcon={<i className="far fa-star"></i>}
          halfIcon={<i className="fa fa-star-half-alt"></i>}
          fullIcon={<i className="fa fa-star"></i>}
          activeColor="#ffd700"
        />
      </div>
      <div className="box">
        <p>How would you rate our Physio ?</p>
        <ReactStars
          className={`stars`}
          count={5}
          onChange={(value) =>
            setSecondRatings(value)
          }
          size={50}
          isHalf={false}
          emptyIcon={<i className="far fa-star"></i>}
          halfIcon={<i className="fa fa-star-half-alt"></i>}
          fullIcon={<i className="fa fa-star"></i>}
          activeColor="#ffd700"
        />
      </div>
    </div>
  );
};

export default Ratings;
