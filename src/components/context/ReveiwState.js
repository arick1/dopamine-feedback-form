import { useState } from "react";
import ReviewContext from "./ReviewContext";

const ReviewState = (props) => {
  // taking info for the ratings part
  const [firstRatings, setFirstRatings] = useState(0);
  const [secondRatings, setSecondRatings] = useState(0);
//   console.log(firstRatings, secondRatings);

  // taking info for the thumbsUp part
  const [isLikeFirst, setIsLikeFirst] = useState(false);
  const [isLikeSecond, setIsLikeSecond] = useState(false);
  const [isDislikeFirst, setIsDislikeFirst] = useState(false);
  const [isDislikeSecond, setIsDislikeSecond] = useState(false);

  // taking info for the review
  const [review, setReview] = useState("");
  // console.log(review);

  return (
    <ReviewContext.Provider
      value={{
        setFirstRatings,
        setSecondRatings,
        setIsLikeFirst,
        setIsLikeSecond,
        setIsDislikeFirst,
        setIsDislikeSecond,
        setReview,
        firstRatings,
        secondRatings,
        isLikeFirst,
        isLikeSecond,
        isDislikeFirst,
        isDislikeSecond,
        review
      }
      }
    >
      {props.children}
    </ReviewContext.Provider>
  );
};

export default ReviewState;
