import { useContext } from "react";
import { BsHandThumbsDown, BsHandThumbsUp } from "react-icons/bs";
import ReveiwContext from "../context/ReviewContext";
import "./Like.css";

const Like = () => {
  // const [isLikeFirst, setIsLikeFirst] = useState(false);
  // const [isLikeSecond, setIsLikeSecond] = useState(false);
  // const [isDislikeFirst, setIsDislikeFirst] = useState(false);
  // const [isDislikeSecond, setIsDislikeSecond] = useState(false);
const {setIsLikeFirst,
  setIsLikeSecond,
  setIsDislikeFirst,
  setIsDislikeSecond,
  isLikeFirst,
  isLikeSecond,
  isDislikeFirst,
  isDislikeSecond} = useContext(ReveiwContext);
  

  // ---------thumbs up --------------
  const thumbsUpFirst = () => {
    setIsLikeFirst(true);
    if (isDislikeFirst) {
      setIsDislikeFirst(false);
    }
  };
  
  const thumbsUpSecond = () => {
    setIsLikeSecond(true);
    if (isDislikeSecond) {
      setIsDislikeSecond(false);
    }
  };

  // -------thumbs down------------------
  const thumbsDownFirst = () => {
    setIsDislikeFirst(true);
    if (isLikeFirst) {
      setIsLikeFirst(false);
    }
  };

  const thumbsDownSecond = () => {
    setIsDislikeSecond(true);
    if (isLikeSecond) {
      setIsLikeSecond(false);
    }
  };

  return (
    <div className="like">
      <div className="box">
        <p>Was your treatment session were on time ?</p>
        <div className="icon">
          <div className={`thumbs ${isDislikeFirst && "red"}`}>
            <BsHandThumbsDown onClick={thumbsDownFirst} />
            <p>No</p>
          </div>
          <div className={`thumbs ${isLikeFirst && "blue"}`}>
            <BsHandThumbsUp onClick={thumbsUpFirst} />
            <p>Yes</p>
          </div>
        </div>
      </div>
      <div className="box">
        <p>Will you recommend synergy to your friends & family ?</p>
        <div className="icon">
          <div className={`thumbs ${isDislikeSecond && "red"}`}>
            <BsHandThumbsDown onClick={thumbsDownSecond} />
            <p>No</p>
          </div>
          <div className={`thumbs ${isLikeSecond && "blue"}`}>
            <BsHandThumbsUp onClick={thumbsUpSecond} />
            <p>Yes</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Like;
