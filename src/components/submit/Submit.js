import { useContext } from "react";
import { useNavigate } from 'react-router-dom';
import ReveiwContext from "../context/ReviewContext";
import "./Submit.css";

const Submit = () => {
    const navigate = useNavigate();

    const {review, setReview,firstRatings,
        secondRatings,
        isLikeFirst,
        isLikeSecond,
        isDislikeFirst,
        isDislikeSecond} = useContext(ReveiwContext);

    const submitIt = () => {
        if(firstRatings && secondRatings && (isLikeFirst || isDislikeFirst)&& (isLikeSecond || isDislikeSecond)) {
            console.log("submit successful");
            navigate("/success");
        } else {
            alert("You have to fill all the inputs");
        }
    }
    
    return (
        <div className='submit'>
            <div className="box">
            <label>Your words for Synergy</label>
            <input type="text" placeholder='Your thoughts...' onChange={(e)=>setReview(e.target.value)}/>
            <br />
            <input type="submit" value="Submit" onClick={submitIt}/>
            </div>
        </div>
    );
};

export default Submit;