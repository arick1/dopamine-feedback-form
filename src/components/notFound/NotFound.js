import { Link } from "react-router-dom";
import "./NotFound.css";

const NotFound = () => {
    return (
        <div className='not_found'>
            <h2>404 Error !</h2>
            <h4>Your desired route isn't found.</h4>
            <h3>Wanna go <Link to="/">Home</Link> ? </h3>
        </div>
    );
};

export default NotFound;