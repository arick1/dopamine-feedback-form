import { useContext } from "react";
import { Navigate } from "react-router-dom";
import ReveiwContext from "../context/ReviewContext";

const Protected = ({ children }) => {

    const {firstRatings,
        secondRatings,
        isLikeFirst,
        isLikeSecond,
        isDislikeFirst,
        isDislikeSecond} = useContext(ReveiwContext);

    if (!(firstRatings && secondRatings && (isLikeFirst || isDislikeFirst)&& (isLikeSecond || isDislikeSecond))) {
        return <Navigate to="/" replace/>;
    }

    return children;
};

export default Protected;
