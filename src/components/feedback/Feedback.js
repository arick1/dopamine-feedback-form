import Like from '../like/Like';
import Ratings from '../ratings/Ratings';
import Submit from '../submit/Submit';

const Feedback = () => {
    return (
        <div className='feedback'>
            <Ratings />
            <Like />
            <Submit />
        </div>
    );
};

export default Feedback;