import "./Header.css";

const Header = () => {
  return (
    <div className="header">
      <h2>Dopamine Feedback Form</h2>
    </div>
  );
};

export default Header;
