import Lottie from "react-lottie-player";
import Tick from "../asset/Success.json";
import "./Success.css";

const Success = () => {
    
    return (
        <div className="success">
            <Lottie
        loop={false}
        animationData={Tick}
        play
        style={{ width: 250, height: 250 }}
        />
        <h1>Congratulations !!</h1>
        <h4>Form Submitted Successfully</h4>

        </div>
    );
};

export default Success;